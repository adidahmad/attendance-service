package jwt

import (
	"crypto/rand"
	"encoding/hex"
	"errors"
	"gitlab.com/adidahmad/attendance-service/config"
	"strings"
	"time"

	jwt "github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo/v4"
)

// JWTManager handles JWT generation and verification.
type JWTManager struct {
	secretKey []byte
}

// NewJWTManager creates a new JWTManager instance with the provided secret key.
func NewJWTManager(secretKey string) *JWTManager {
	return &JWTManager{
		secretKey: []byte(secretKey),
	}
}

// GenerateToken generates a new JWT token for the given claims.
func (j *JWTManager) GenerateToken(data *config.JwtCustomClaims) (string, error) {
	tokenId, err := generateRandomBytes(32)
	if err != nil {
		return "", err
	}

	issuedAt := time.Now()

	data.Issuer = "http://localhost"
	data.Id = hex.EncodeToString(tokenId)
	data.IssuedAt = issuedAt.Unix()
	data.ExpiresAt = issuedAt.Add(time.Minute * 15).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, data)

	return token.SignedString(j.secretKey)
}

// ParseToken parses and verifies a JWT token, returning the claims if valid.
func (j *JWTManager) ParseToken(tokenString string) (jwt.Claims, error) {
	token, err := jwt.ParseWithClaims(tokenString, jwt.MapClaims{}, func(token *jwt.Token) (interface{}, error) {
		return j.secretKey, nil
	})

	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	}

	return nil, jwt.ErrSignatureInvalid
}

func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)

	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (j *JWTManager) GetUserId(c echo.Context) (int, error) {
	// data := c.Get("user").(*jwt.Token)
	tokenEncoded := c.Request().Header.Get("Authorization")
	t := strings.Split(tokenEncoded, " ")
	if t[1] == "" {
		return 0, errors.New("Required Authorization Header")
	}

	tokenClaims, err := j.ParseToken(t[1])
	if err != nil {
		return 0, err
	}

	claims := tokenClaims.(jwt.MapClaims)

	return int(claims["user_id"].(float64)), nil
}
