package server

import (
	"context"
	"gitlab.com/adidahmad/attendance-service/ent"
	"net/http"
	"os"
	"os/signal"
	"time"

	customValidator "gitlab.com/adidahmad/attendance-service/pkg/validator"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	mw "github.com/labstack/echo/v4/middleware"
)

// Start initializes and starts the Echo server.
func Start(e *echo.Echo, port string, dbConn *ent.Client) {
	e.Pre(mw.RemoveTrailingSlash())

	// Middleware
	e.Use(middleware.Logger())
	e.Use(mw.Secure())
	e.Use(mw.CORSWithConfig(mw.CORSConfig{
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
	}))

	e.Validator = &customValidator.BodyRequestValidator{
		Validator: CustomValidator(),
	}

	e.GET("/api", func(c echo.Context) error {
		return c.String(http.StatusOK, "Welcome to the Attendance Service API")
	})

	go func() {
		if err := e.Start(port); err != nil {
			dbConn.Close()
			panic(err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := e.Shutdown(ctx); err != nil {
		dbConn.Close()
		panic(err)
	}
}

func CustomValidator() *validator.Validate {
	customValidator := validator.New()
	return customValidator
}
