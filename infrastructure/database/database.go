package database

import (
	"context"
	"fmt"

	"gitlab.com/adidahmad/attendance-service/config"
	"gitlab.com/adidahmad/attendance-service/ent"

	"entgo.io/ent/dialect"
	_ "github.com/go-sql-driver/mysql"
)

// InitDatabase initializes the MySQL database connection and schema.
func InitDatabase(dbConf config.DatabaseConfig) (*ent.Client, error) {
	client, err := ent.Open(dialect.MySQL, fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true", dbConf.User, dbConf.Pass, dbConf.Host, dbConf.Port, dbConf.Name))
	if err != nil {
		return nil, err
	}

	if err := client.Schema.Create(context.Background()); err != nil {
		return nil, err
	}

	return client, nil
}
