package main

import (
	"fmt"
	attendanceHandler "gitlab.com/adidahmad/attendance-service/app/handlers/attendance"
	authHandler "gitlab.com/adidahmad/attendance-service/app/handlers/auth"
	userHandler "gitlab.com/adidahmad/attendance-service/app/handlers/user"
	"gitlab.com/adidahmad/attendance-service/app/middleware"
	"gitlab.com/adidahmad/attendance-service/config"
	"gitlab.com/adidahmad/attendance-service/infrastructure/database"
	"gitlab.com/adidahmad/attendance-service/internal/attendance"
	"gitlab.com/adidahmad/attendance-service/internal/auth"
	"gitlab.com/adidahmad/attendance-service/internal/notification"
	"gitlab.com/adidahmad/attendance-service/internal/scheduler"
	"gitlab.com/adidahmad/attendance-service/internal/user"
	"gitlab.com/adidahmad/attendance-service/pkg/jwt"
	"gitlab.com/adidahmad/attendance-service/pkg/server"

	echoJwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
)

func main() {
	// Load application configuration
	config.LoadConfig("./config.yaml")

	// Initialize the database connection
	dbConn, err := database.InitDatabase(config.DatabaseConf)
	if err != nil {
		panic(err)
	}

	// Initialize JWT with the secret key
	jwtManager := jwt.NewJWTManager(config.AppConf.JWTSecret)

	userRepo := user.NewUserRepository(dbConn)
	authRepo := auth.NewAuthRepository(dbConn)
	attendanceRepo := attendance.NewAttendanceRepository(dbConn)

	// Create the User use case
	authUsecase := auth.NewAuthUseCase(authRepo, jwtManager)
	userUsecase := user.NewUserUseCase(userRepo)
	attendanceUsecase := attendance.NewAttendanceUseCase(attendanceRepo)

	// Initialize API handlers
	userHandler := userHandler.NewUserHandler(userUsecase)
	authHandler := authHandler.NewAuthHandler(authUsecase)
	attendanceHandler := attendanceHandler.NewAttendanceHandler(*attendanceUsecase, jwtManager)

	reminderService := notification.NewReminderService(config.SmtpConf)

	// Create the Scheduler and start it.
	jobScheduler := scheduler.NewScheduler(reminderService, attendanceUsecase, userUsecase)
	jobScheduler.Start()

	// Initialize Echo server
	e := echo.New()

	// Initialize internal middleware
	mw := middleware.New(jwtManager)

	// Set up API routes
	apiGroup := e.Group("/api")

	// Authentication routes
	authGroup := apiGroup.Group("/auth")
	authGroup.POST("/login", authHandler.Login)
	authGroup.POST("/register", authHandler.Register)

	// Attendance routes
	attendanceGroup := apiGroup.Group("/attendance", echoJwt.JWT([]byte(config.AppConf.JWTSecret)))
	attendanceGroup.POST("/clock-in", attendanceHandler.ClockIn)
	attendanceGroup.POST("/clock-out", attendanceHandler.ClockOut)
	attendanceGroup.GET("/:id", attendanceHandler.GetAttendanceByUserId)
	attendanceGroup.DELETE("/:id", attendanceHandler.DeleteAttendance)

	// User routes
	userGroup := apiGroup.Group("/user", echoJwt.JWT([]byte(config.AppConf.JWTSecret)), mw.AccessControl())
	userGroup.POST("", userHandler.CreateUser)
	userGroup.GET("", userHandler.GetUsers)
	userGroup.PUT("/:id", userHandler.UpdateUser)
	userGroup.DELETE("/:id", userHandler.DeleteUser)

	// Start the Echo server
	server.Start(e, fmt.Sprintf(":%s", config.AppConf.Port), dbConn)
	defer jobScheduler.Stop()
}
