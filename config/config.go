package config

import (
	"log"

	"github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
)

var (
	AppConf      AppConfig
	DatabaseConf DatabaseConfig
	SmtpConf     SmtpConfig
	JwtConfig    middleware.JWTConfig
)

// Config holds the application configuration.
type (
	AppConfig struct {
		Port      string `mapstructure:"port"`
		JWTSecret string `mapstructure:"jwt_secret"`
	}

	DatabaseConfig struct {
		Host string `mapstructure:"db_host"`
		User string `mapstructure:"db_user"`
		Pass string `mapstructure:"db_pass"`
		Name string `mapstructure:"db_name"`
		Port int    `mapstructure:"db_port"`
	}

	SmtpConfig struct {
		SmtpServer   string `mapstructure:"smtp_server"`
		SmtpUsername string `mapstructure:"smtp_username"`
		SmtpPassword string `mapstructure:"smtp_password"`
		SmtpPort     int    `mapstructure:"smtp_port"`
	}
)

type JwtCustomClaims struct {
	jwt.StandardClaims
	UserId int    `json:"user_id,omitempty"`
	Name   string `json:"name,omitempty"`
	Email  string `json:"email,omitempty"`
	Role   string `json:"role,omitempty"`
	Status string `json:"status,omitempty"`
}

func init() {
	JwtConfig = middleware.JWTConfig{
		Claims:     &JwtCustomClaims{},
		SigningKey: AppConf.JWTSecret,
	}
}

// LoadConfig loads application configuration from a YAML file.
func LoadConfig(configFile string) error {
	viper.SetConfigFile(configFile)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading configuration file: %s", err)
		return nil
	}

	err := viper.Unmarshal(&AppConf)
	if err != nil {
		log.Fatalf("Unable to decode into app struct: %s", err)
		return nil
	}

	err = viper.Unmarshal(&DatabaseConf)
	if err != nil {
		log.Fatalf("Unable to decode into database struct: %s", err)
		return nil
	}

	err = viper.Unmarshal(&SmtpConf)
	if err != nil {
		log.Fatalf("Unable to decode into smtp struct: %s", err)
		return nil
	}

	return nil
}
