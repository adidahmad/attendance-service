package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

// Fields of the Users.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").
			Unique(),
		field.String("name").
			MaxLen(255),
		field.String("email").
			Unique().
			MaxLen(255),
		field.String("password").
			MaxLen(255),
		field.Enum("status").
			Values("active", "inactive").
			Default("active"),
		field.Enum("role").
			Values("employee", "admin").
			Default("employee"),
		field.Time("created_at").
			Default(time.Now).
			Immutable(),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return nil
}
