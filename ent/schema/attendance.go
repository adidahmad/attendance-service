package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Attendance holds the schema definition for the Attendance entity.
type Attendance struct {
	ent.Schema
}

// Fields of the Attendance.
func (Attendance) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").
			Unique(),
		field.Int("user_id"),
		field.Time("clockin_time").
			Nillable().
			Optional(),
		field.Time("clockout_time").
			Nillable().
			Optional(),
		field.Time("date").
			SchemaType(map[string]string{
				dialect.MySQL: "date",
			}),
	}
}

// Edges of the Attendance.
func (Attendance) Edges() []ent.Edge {
	return nil
}

func (Attendance) Indexes() []ent.Index {
	return []ent.Index{
		// Unique index on user_id and date to ensure one entry per user per day.
		index.Fields("user_id", "date").
			Unique(),
	}
}
