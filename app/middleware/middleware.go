package middleware

import (
	"errors"
	"net/http"
	"strings"

	"gitlab.com/adidahmad/attendance-service/app/util"
	jwtHelper "gitlab.com/adidahmad/attendance-service/pkg/jwt"

	jwt "github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo/v4"
)

type Controller struct {
	jwtManager *jwtHelper.JWTManager
}

//New Construct auth API controller
func New(jwtManager *jwtHelper.JWTManager) *Controller {
	return &Controller{
		jwtManager: jwtManager,
	}
}

func (ctrl *Controller) AccessControl() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			tokenEncoded := c.Request().Header.Get("Authorization")
			t := strings.Split(tokenEncoded, " ")
			if t[1] == "" {
				err := errors.New("Required Authorization Header")

				code := http.StatusForbidden
				result := util.GetResultMapping(code)
				result.Errors = append(result.Errors, err.Error())

				return c.JSON(code, util.NewResponse(result, nil))
			}

			tokenClaims, err := ctrl.jwtManager.ParseToken(t[1])
			if err != nil {
				code := http.StatusForbidden
				result := util.GetResultMapping(code)
				result.Errors = append(result.Errors, err.Error())

				return c.JSON(code, util.NewResponse(result, nil))
			}

			claims, ok := tokenClaims.(jwt.MapClaims)
			if !ok {
				code := http.StatusInternalServerError
				result := util.GetResultMapping(code)
				result.Errors = append(result.Errors, "Error getting claims")

				return c.JSON(code, util.NewResponse(result, nil))
			}

			role, ok := claims["role"].(string)
			if !ok {
				code := http.StatusInternalServerError
				result := util.GetResultMapping(code)
				result.Errors = append(result.Errors, "Error getting claims")

				return c.JSON(code, util.NewResponse(result, nil))
			}

			if role != "admin" {
				code := http.StatusForbidden
				result := util.GetResultMapping(code)
				result.Errors = append(result.Errors, "Not allowed to perform this action")

				return c.JSON(code, util.NewResponse(result, nil))
			}

			return next(c)
		}
	}
}
