// api/handlers/attendance_handler.go

package attendance

import (
	"gitlab.com/adidahmad/attendance-service/app/util"
	"gitlab.com/adidahmad/attendance-service/internal/attendance"
	jwtHelper "gitlab.com/adidahmad/attendance-service/pkg/jwt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type AttendanceHandler struct {
	attendanceUC attendance.AttendanceUseCase
	jwtManager   *jwtHelper.JWTManager
}

func NewAttendanceHandler(attendanceUC attendance.AttendanceUseCase, jwtManager *jwtHelper.JWTManager) *AttendanceHandler {
	return &AttendanceHandler{
		attendanceUC: attendanceUC,
		jwtManager:   jwtManager,
	}
}

func (h *AttendanceHandler) ClockIn(c echo.Context) error {
	// Perform the clock-in using the AttendanceUseCase
	userId, err := h.jwtManager.GetUserId(c)
	if err != nil {
		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	if err := h.attendanceUC.ClockIn(userId); err != nil {
		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"message": "successfully",
	})
}

func (h *AttendanceHandler) ClockOut(c echo.Context) error {
	// Perform the clock-out using the AttendanceUseCase
	userId, err := h.jwtManager.GetUserId(c)
	if err != nil {
		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	if err := h.attendanceUC.ClockOut(userId); err != nil {
		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"message": "successfully",
	})
}

func (h *AttendanceHandler) GetAttendanceByUserId(c echo.Context) error {
	id := c.Param("id")
	userId, _ := strconv.Atoi(id)

	users, err := h.attendanceUC.GetAttendancesByUserId(userId)
	if err != nil {
		code := http.StatusInternalServerError
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	return c.JSON(http.StatusOK, users)
}

func (h *AttendanceHandler) DeleteAttendance(c echo.Context) error {
	// Parse and validate the request to delete a user
	id := c.Param("id")
	userId, _ := strconv.Atoi(id)

	// Delete the user using the UserUseCase
	if err := h.attendanceUC.DeleteUser(userId); err != nil {
		code := http.StatusInternalServerError
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"message": "successfully",
	})
}
