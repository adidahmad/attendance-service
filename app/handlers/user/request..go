package user

type createUserRequest struct {
	Name     string `json:"name" validate:"required"`
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
	Status   string `json:"status" validate:"required"`
	Role     string `json:"role" validate:"required"`
}

type updateUserRequest struct {
	Name     string `json:"name" validate:"required"`
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
	Status   string `json:"status" validate:"required"`
	Role     string `json:"role" validate:"required"`
}
