package user

import (
	"gitlab.com/adidahmad/attendance-service/app/util"
	"gitlab.com/adidahmad/attendance-service/internal/user"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type UserHandler struct {
	userUsecase *user.UserUseCase
}

func NewUserHandler(userUsecase *user.UserUseCase) *UserHandler {
	return &UserHandler{userUsecase: userUsecase}
}

func (h *UserHandler) CreateUser(c echo.Context) error {
	// Parse and validate the registration request
	bodyRequest := new(createUserRequest)
	if err := c.Bind(bodyRequest); err != nil {
		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	if err := c.Validate(bodyRequest); err != nil {
		code := http.StatusBadRequest
		errors := util.BuildErrorBodyRequestValidator(err)
		result := util.GetResultMapping(code)
		result.Errors = errors

		return c.JSON(code, util.NewResponse(result, nil))
	}

	// Create a User struct from the request data
	req := &user.CreateRequestModel{
		Name:     bodyRequest.Name,
		Email:    bodyRequest.Email,
		Password: bodyRequest.Password,
		Status:   bodyRequest.Status,
		Role:     bodyRequest.Role,
	}

	res, err := h.userUsecase.Create(req)
	if err != nil {
		code := http.StatusInternalServerError
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	return c.JSON(http.StatusCreated, res)
}

func (h *UserHandler) GetUsers(c echo.Context) error {
	// Get the list of users using the UserUseCase
	users, err := h.userUsecase.GetUsers()
	if err != nil {
		code := http.StatusInternalServerError
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, err.Error())
	}

	return c.JSON(http.StatusOK, users)
}

func (h *UserHandler) UpdateUser(c echo.Context) error {
	// Parse and validate the request to update a user
	bodyRequest := new(updateUserRequest)
	if err := c.Bind(bodyRequest); err != nil {
		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	if err := c.Validate(bodyRequest); err != nil {
		code := http.StatusBadRequest
		errors := util.BuildErrorBodyRequestValidator(err)
		result := util.GetResultMapping(code)
		result.Errors = errors

		return c.JSON(code, util.NewResponse(result, nil))
	}

	id := c.Param("id")
	userId, _ := strconv.Atoi(id)

	req := &user.UpdateRequestModel{
		Name:     bodyRequest.Name,
		Email:    bodyRequest.Email,
		Password: bodyRequest.Password,
		Status:   bodyRequest.Status,
		Role:     bodyRequest.Role,
	}

	// Update the user using the UserUseCase
	user, err := h.userUsecase.UpdateUser(userId, req)
	if err != nil {
		code := http.StatusInternalServerError
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	return c.JSON(http.StatusOK, user)
}

func (h *UserHandler) DeleteUser(c echo.Context) error {
	// Parse and validate the request to delete a user
	id := c.Param("id")
	userId, _ := strconv.Atoi(id)

	// Delete the user using the UserUseCase
	if err := h.userUsecase.DeleteUser(userId); err != nil {
		code := http.StatusInternalServerError
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"message": "successfully",
	})
}
