// api/handlers/auth_handler.go

package auth

import (
	"gitlab.com/adidahmad/attendance-service/app/util"
	"gitlab.com/adidahmad/attendance-service/internal/auth"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

type AuthHandler struct {
	authUC    auth.AuthUseCase
	jwtSecret string
}

func NewAuthHandler(authUc auth.AuthUseCase) *AuthHandler {
	return &AuthHandler{
		authUC: authUc,
	}
}

func (h *AuthHandler) Login(c echo.Context) error {
	bodyRequest := new(loginRequest)
	if err := c.Bind(bodyRequest); err != nil {
		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	if err := c.Validate(bodyRequest); err != nil {
		code := http.StatusBadRequest
		errors := util.BuildErrorBodyRequestValidator(err)
		result := util.GetResultMapping(code)
		result.Errors = errors

		return c.JSON(code, util.NewResponse(result, nil))
	}

	// Authenticate the user using the AuthUseCase
	res, err := h.authUC.Authenticate(bodyRequest.Email, bodyRequest.Password)
	if err != nil {
		if strings.Contains(err.Error(), "invalid email/password") {
			code := http.StatusUnauthorized
			result := util.GetResultMapping(code)
			result.Errors = append(result.Errors, err.Error())

			return c.JSON(code, util.NewResponse(result, nil))
		}

		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	// Return the JWT token in the response
	return c.JSON(http.StatusOK, &res)
}

func (h *AuthHandler) Register(c echo.Context) error {
	// Parse and validate the registration request
	bodyRequest := new(registerRequest)
	if err := c.Bind(bodyRequest); err != nil {
		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}

	if err := c.Validate(bodyRequest); err != nil {
		code := http.StatusBadRequest
		errors := util.BuildErrorBodyRequestValidator(err)
		result := util.GetResultMapping(code)
		result.Errors = errors

		return c.JSON(code, util.NewResponse(result, nil))
	}

	// Create a User struct from the request data
	req := auth.RegisterRequestModel{
		Name:     bodyRequest.Name,
		Email:    bodyRequest.Email,
		Password: bodyRequest.Password,
	}
	res, err := h.authUC.Register(req)
	if err != nil {
		code := http.StatusBadRequest
		result := util.GetResultMapping(code)
		result.Errors = append(result.Errors, err.Error())

		return c.JSON(code, util.NewResponse(result, nil))
	}
	// Call the UserUseCase to register the user
	// Return a response based on the success or failure of the registration
	return c.JSON(http.StatusCreated, res)
}
