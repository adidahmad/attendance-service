package util

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"

	"github.com/go-playground/validator"
)

type Result struct {
	Code    int      `json:"code"`
	Message string   `json:"message"`
	Errors  []string `json:"errors,omitempty"`
}

type Response struct {
	Result
	Data interface{} `json:"data,omitempty"`
}

func NewResponse(res Result, data interface{}) Response {
	return Response{
		Result: res,
		Data:   data,
	}
}

type ItemsResponse struct {
	Items interface{} `json:"items"`
}

type ListResponse struct {
	Items interface{} `json:"items"`
	Meta  Meta        `json:"_meta"`
}

type Meta struct {
	TotalCount  int64 `json:"total_count"`
	PageCount   int64 `json:"page_count"`
	CurrentPage int64 `json:"current_page"`
	PerPage     int64 `json:"per_page"`
}

var mapping = map[int]Result{
	200: {Code: http.StatusOK, Message: strings.ToLower(http.StatusText(http.StatusOK))},
	201: {Code: http.StatusCreated, Message: strings.ToLower(http.StatusText(http.StatusCreated))},
	400: {Code: http.StatusBadRequest, Message: strings.ToLower(http.StatusText(http.StatusBadRequest))},
	401: {Code: http.StatusUnauthorized, Message: strings.ToLower(http.StatusText(http.StatusUnauthorized))},
	403: {Code: http.StatusForbidden, Message: strings.ToLower(http.StatusText(http.StatusForbidden))},
	404: {Code: http.StatusNotFound, Message: strings.ToLower(http.StatusText(http.StatusNotFound))},
	500: {Code: http.StatusInternalServerError, Message: strings.ToLower(http.StatusText(http.StatusInternalServerError))},
}

func GetResultMapping(code int) Result {
	return mapping[code]
}

func BuildErrorBodyRequestValidator(err error) []string {
	var errors []string
	for _, err := range err.(validator.ValidationErrors) {
		message := ErrorBodyRequestValidatorString(err.Tag(), err.Field(), err.Param())
		errors = append(errors, message)
	}
	return errors
}

func ErrorBodyRequestValidatorString(tag string, field string, param string) string {
	error := ""
	switch tag {
	case "email":
		error = fmt.Sprintf("%s: incorrect format", SnakeToCamel(field))
	case "required":
		error = fmt.Sprintf("%s: required field", SnakeToCamel(field))
	}

	return error
}

func SnakeToCamel(str string) string {
	matchFirstCap := regexp.MustCompile("(.)([A-Z][a-z]+)")
	matchAllCap := regexp.MustCompile("([a-z0-9])([A-Z])")

	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")

	return strings.ToLower(snake)
}
