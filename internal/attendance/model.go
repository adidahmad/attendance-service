// internal/app/attendance/model.go

package attendance

import "time"

type AttendanceModel struct {
	ID           int        `json:"id"`
	UserID       int        `json:"user_id"`
	ClockInTime  *time.Time `json:"clockin_time,omitempty"`
	ClockOutTime *time.Time `json:"clockout_time,omitempty"`
	Date         time.Time  `json:"date"`
}
