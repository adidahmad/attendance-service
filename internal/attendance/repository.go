// internal/app/attendance/repository.go

package attendance

import (
	"context"
	"errors"
	"gitlab.com/adidahmad/attendance-service/ent"
	"gitlab.com/adidahmad/attendance-service/ent/attendance"
	"time"

	"github.com/davecgh/go-spew/spew"
)

type AttendanceRepository interface {
	ClockIn(userId int, clockInTime time.Time, date time.Time) error
	ClockOut(userId int, clockOutTime time.Time, date time.Time) error
	GetListByDate(date time.Time) ([]*AttendanceModel, error)
	GetListByUserId(userId int) ([]*AttendanceModel, error)
	Delete(id int) error
	// UpdateAttendance(attendance Attendance) (Attendance, error)
	// DeleteAttendance(attendanceID int) error
}

type AttendanceRepositoryImpl struct {
	client *ent.Client
}

func NewAttendanceRepository(client *ent.Client) AttendanceRepository {
	return &AttendanceRepositoryImpl{
		client: client,
	}
}

func (r *AttendanceRepositoryImpl) ClockIn(userId int, clockInTime time.Time, date time.Time) error {
	// Implement the logic to record a clock-in time in the database using Ent.
	ctx := context.Background()

	// Check if there is already a clock-in entry for the user and date
	_, err := r.client.Attendance.Query().
		Where(attendance.And(
			attendance.UserID(userId),
			attendance.ClockinTimeIsNil(),
			attendance.Date(date),
		)).
		Only(ctx)

	if err != nil {
		if ent.IsNotFound(err) {
			spew.Dump(err)
			_, errCreate := r.client.Attendance.Create().
				SetUserID(userId).
				SetClockinTime(clockInTime).
				SetDate(date).
				Save(ctx)
			if errCreate != nil {
				// Handle the error if the creation fails
				return errCreate
			}
		}

		return err
	}

	return errors.New("already clock in")
}

func (r *AttendanceRepositoryImpl) ClockOut(userId int, clockOutTime time.Time, date time.Time) error {
	// Implement the logic to record a clock-out time in the database using Ent.
	ctx := context.Background()

	// Check if there is already a clock-in entry for the user and date
	existingEntry, err := r.client.Attendance.Query().
		Where(attendance.And(
			attendance.UserID(userId),
			attendance.ClockoutTimeIsNil(),
			attendance.Date(date),
		)).
		Only(ctx)

	if err != nil {
		if ent.IsNotFound(err) {
			// No existing entry, so create a new one
			_, err := r.client.Attendance.Create().
				SetUserID(userId).
				SetClockoutTime(clockOutTime).
				SetDate(date).
				Save(ctx)
			if err != nil {
				// Handle the error if the creation fails
				return err
			}
		}

		return err
	} else {
		// An entry already exists, so update the clock-in time
		_, err := existingEntry.Update().
			SetClockoutTime(clockOutTime).
			Save(ctx)
		if err != nil {
			// Handle the error if the update fails
			return err
		}
	}

	return errors.New("already clock out")
}

func (r *AttendanceRepositoryImpl) GetListByDate(date time.Time) ([]*AttendanceModel, error) {
	ctx := context.Background()

	entity, err := r.client.Attendance.Query().
		Where(attendance.And(
			attendance.Date(date),
		)).
		All(ctx)

	return entitiesToModels(entity), err
}

func (r *AttendanceRepositoryImpl) GetListByUserId(userId int) ([]*AttendanceModel, error) {
	ctx := context.Background()

	entity, err := r.client.Attendance.Query().
		Where(attendance.And(
			attendance.UserID(userId),
		)).
		All(ctx)

	return entitiesToModels(entity), err
}

func (r *AttendanceRepositoryImpl) Delete(id int) error {
	ctx := context.Background()

	err := r.client.Attendance.DeleteOneID(id).
		Exec(ctx)

	return err
}

func entitiesToModels(entAttendances []*ent.Attendance) []*AttendanceModel {
	var attendances []*AttendanceModel
	for _, entAttendance := range entAttendances {
		attendances = append(attendances, entityToModel(entAttendance))
	}

	return attendances
}

func entityToModel(eu *ent.Attendance) *AttendanceModel {
	return &AttendanceModel{
		ID:           eu.ID,
		UserID:       eu.UserID,
		ClockInTime:  eu.ClockinTime,
		ClockOutTime: eu.ClockoutTime,
		Date:         *&eu.Date,
	}
}
