// internal/app/attendance/usecase.go

package attendance

import (
	"time"
)

type AttendanceUseCase struct {
	attendanceRepo AttendanceRepository
}

func NewAttendanceUseCase(attendanceRepo AttendanceRepository) *AttendanceUseCase {
	return &AttendanceUseCase{attendanceRepo: attendanceRepo}
}

func (uc *AttendanceUseCase) ClockIn(userID int) error {
	date := time.Now().Truncate(24 * time.Hour)

	return uc.attendanceRepo.ClockIn(userID, time.Now(), date)
}

func (uc *AttendanceUseCase) ClockOut(userID int) error {
	date := time.Now().Truncate(24 * time.Hour)

	return uc.attendanceRepo.ClockOut(userID, time.Now(), date)
}

func (uc *AttendanceUseCase) GetAttendances(date time.Time) ([]*AttendanceModel, error) {
	return uc.attendanceRepo.GetListByDate(date.Truncate(24 * time.Hour))
}

func (uc *AttendanceUseCase) GetAttendancesByUserId(userId int) ([]*AttendanceModel, error) {
	return uc.attendanceRepo.GetListByUserId(userId)
}

func (uc *AttendanceUseCase) DeleteUser(id int) error {
	return uc.attendanceRepo.Delete(id)
}
