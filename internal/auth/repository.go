// internal/app/auth/repository.go

package auth

import (
	"context"
	"gitlab.com/adidahmad/attendance-service/ent"
	"gitlab.com/adidahmad/attendance-service/ent/user"
	userEntity "gitlab.com/adidahmad/attendance-service/ent/user"
)

type AuthRepository interface {
	GetUserByEmail(email string) (*UserModel, error)
	CreateUser(data RegisterRequestModel) (*UserModel, error)
}

type AuthRepositoryImpl struct {
	client *ent.Client
}

func NewAuthRepository(client *ent.Client) AuthRepository {
	return &AuthRepositoryImpl{
		client: client,
	}
}

func (r *AuthRepositoryImpl) GetUserByEmail(email string) (*UserModel, error) {
	ctx := context.Background()

	// Query the user entity by email using the Ent generated code
	entUser, err := r.client.User.Query().
		Where(user.EmailEQ(email)).
		Only(ctx)

	if err != nil {
		return nil, err
	}

	return entityToModel(entUser), nil // Authentication successful
}

func (r *AuthRepositoryImpl) CreateUser(data RegisterRequestModel) (*UserModel, error) {
	ctx := context.Background()

	entUser, err := r.client.User.
		Create().
		SetName(data.Name).
		SetEmail(data.Email).
		SetPassword(data.Password).
		SetStatus(userEntity.Status(data.Status)).
		SetRole(userEntity.Role(data.Role)).
		Save(ctx)

	if err != nil {
		return nil, err
	}

	return entityToModel(entUser), nil
}

func entityToModel(eu *ent.User) *UserModel {
	return &UserModel{
		ID:        eu.ID,
		Name:      eu.Name,
		Email:     eu.Email,
		Password:  eu.Password,
		Status:    userEntity.Status.String(eu.Status),
		Role:      userEntity.Role.String(eu.Role),
		CreatedAt: eu.CreatedAt,
	}
}
