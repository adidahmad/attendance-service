// internal/app/auth/usecase.go

package auth

import (
	"errors"

	"gitlab.com/adidahmad/attendance-service/config"
	jwtHelper "gitlab.com/adidahmad/attendance-service/pkg/jwt"

	userEntity "gitlab.com/adidahmad/attendance-service/ent/user"

	"github.com/davecgh/go-spew/spew"
	"golang.org/x/crypto/bcrypt"
)

type AuthUseCase struct {
	authRepo   AuthRepository
	jwtManager *jwtHelper.JWTManager
}

func NewAuthUseCase(authRepo AuthRepository, jwtManager *jwtHelper.JWTManager) AuthUseCase {
	return AuthUseCase{
		authRepo:   authRepo,
		jwtManager: jwtManager,
	}
}

func (uc *AuthUseCase) Authenticate(email, password string) (*loginResponse, error) {
	// Implement the authentication logic
	res, err := uc.authRepo.GetUserByEmail(email)
	if err != nil {
		return nil, err
	}

	if res.Status == "inactive" {
		return nil, errors.New("inactive user")
	}

	// Compare the hashed password with the provided password
	if err := bcrypt.CompareHashAndPassword([]byte(res.Password), []byte(password)); err != nil {
		return nil, errors.New("invalid email / password") // Passwords don't match
	}

	data := &config.JwtCustomClaims{
		UserId: res.ID,
		Name:   res.Name,
		Email:  res.Email,
		Role:   res.Role,
		Status: res.Status,
	}
	token, err := uc.jwtManager.GenerateToken(data)
	if err != nil {
		return nil, err
	}

	return &loginResponse{AccessToken: token}, nil
}

func (uc *AuthUseCase) Register(data RegisterRequestModel) (*UserModel, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	data.Password = string(hashedPassword)
	data.Status = userEntity.DefaultStatus.String()
	data.Role = userEntity.DefaultRole.String()

	// Create the user in the database
	createdUser, err := uc.authRepo.CreateUser(data)
	if err != nil {
		spew.Dump(err.Error())
		return nil, err
	}

	return createdUser, nil
}
