// internal/app/user/usecase.go

package user

import (
	entUser "gitlab.com/adidahmad/attendance-service/ent/user"

	"golang.org/x/crypto/bcrypt"
)

type UserUseCase struct {
	userRepo UserRepository
}

func NewUserUseCase(userRepo UserRepository) *UserUseCase {
	return &UserUseCase{userRepo: userRepo}
}

func (uc *UserUseCase) Create(data *CreateRequestModel) (*UserModel, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	data.Password = string(hashedPassword)
	if data.Status == "" {
		data.Status = entUser.DefaultStatus.String()
	}

	if data.Role == "" {
		data.Role = entUser.DefaultRole.String()
	}

	// Create the user in the database
	createdUser, err := uc.userRepo.Create(data)
	if err != nil {
		return nil, err
	}

	return createdUser, nil
}

func (uc *UserUseCase) GetUsers() ([]*UserModel, error) {
	return uc.userRepo.GetList()
}

func (uc *UserUseCase) UpdateUser(userId int, data *UpdateRequestModel) (*UserModel, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	data.Password = string(hashedPassword)
	if data.Status == "" {
		data.Status = entUser.DefaultStatus.String()
	}

	if data.Role == "" {
		data.Role = entUser.DefaultRole.String()
	}

	return uc.userRepo.Update(userId, data)
}

func (uc *UserUseCase) DeleteUser(userId int) error {
	return uc.userRepo.Delete(userId)
}

func (uc *UserUseCase) GetUsersEmailUsernameByIds(userIds []int) ([]GetUserEmailUsernameModel, error) {
	return uc.userRepo.GetUsersEmailUsernameByIds(userIds)
}
