// internal/app/user/repository.go

package user

import (
	"context"
	"gitlab.com/adidahmad/attendance-service/ent"
	userEntity "gitlab.com/adidahmad/attendance-service/ent/user"
)

type UserRepository interface {
	Create(data *CreateRequestModel) (*UserModel, error)
	GetList() ([]*UserModel, error)
	Update(id int, data *UpdateRequestModel) (*UserModel, error)
	Delete(id int) error
	GetUsersEmailUsernameByIds(ids []int) ([]GetUserEmailUsernameModel, error)
	// Add more methods for retrieving and managing users
}

type UserRepositoryImpl struct {
	client *ent.Client
}

func NewUserRepository(client *ent.Client) UserRepository {
	return &UserRepositoryImpl{client: client}
}

func (r *UserRepositoryImpl) Create(data *CreateRequestModel) (*UserModel, error) {
	ctx := context.Background()
	// Create a new user entity using the Ent generated code
	newUser, err := r.client.User.Create().
		SetName(data.Name).
		SetEmail(data.Email).
		SetPassword(data.Password).
		SetStatus(userEntity.Status(data.Status)).
		SetRole(userEntity.Role(data.Role)).
		Save(ctx)

	if err != nil {
		return nil, err
	}

	return entityToModel(newUser), nil
}

func (r *UserRepositoryImpl) GetList() ([]*UserModel, error) {
	ctx := context.Background()

	users, err := r.client.User.Query().
		All(ctx)

	if err != nil {
		return nil, err
	}

	return entitiesToModels(users), nil
}

func (r *UserRepositoryImpl) Update(id int, data *UpdateRequestModel) (*UserModel, error) {
	ctx := context.Background()

	entity, err := r.client.User.Query().
		Where(userEntity.And(userEntity.ID(id))).
		Only(ctx)

	if err != nil {
		return nil, err
	}

	// Use the provided user data to update the user in the database
	updatedUser, err := entity.Update().
		SetName(data.Name).
		SetEmail(data.Email).
		SetPassword(data.Password).
		SetStatus(userEntity.Status(data.Status)).
		SetRole(userEntity.Role(data.Role)).
		Save(ctx)

	if err != nil {
		return nil, err
	}

	return entityToModel(updatedUser), nil
}

func (r *UserRepositoryImpl) Delete(id int) error {
	ctx := context.Background()

	// Delete the user by ID
	err := r.client.User.DeleteOneID(id).
		Exec(ctx)

	return err
}

func (r *UserRepositoryImpl) GetUsersEmailUsernameByIds(ids []int) ([]GetUserEmailUsernameModel, error) {
	ctx := context.Background()

	var result []GetUserEmailUsernameModel
	err := r.client.User.Query().
		Where(userEntity.And(
			userEntity.IDIn(ids...),
		)).
		Select(userEntity.FieldEmail, userEntity.FieldName).
		Scan(ctx, &result)

	return result, err
}

func entitiesToModels(entUsers []*ent.User) []*UserModel {
	var users []*UserModel
	for _, entUser := range entUsers {
		users = append(users, entityToModel(entUser))
	}
	return users
}

func entityToModel(eu *ent.User) *UserModel {
	return &UserModel{
		ID:        eu.ID,
		Name:      eu.Name,
		Email:     eu.Email,
		Password:  eu.Password,
		Status:    userEntity.Status.String(eu.Status),
		Role:      userEntity.Role.String(eu.Role),
		CreatedAt: eu.CreatedAt,
	}
}
