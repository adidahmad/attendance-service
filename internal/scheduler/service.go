// internal/app/scheduler/scheduler.go

package scheduler

import (
	"gitlab.com/adidahmad/attendance-service/internal/attendance"
	"gitlab.com/adidahmad/attendance-service/internal/notification"
	"gitlab.com/adidahmad/attendance-service/internal/user"
	"log"
	"time"

	"github.com/robfig/cron/v3"
)

type Scheduler struct {
	cron         *cron.Cron
	reminderSrv  *notification.ReminderService
	attendanceUc *attendance.AttendanceUseCase
	userUc       *user.UserUseCase
}

func NewScheduler(reminderSrv *notification.ReminderService, attendanceUc *attendance.AttendanceUseCase, userUc *user.UserUseCase) *Scheduler {
	return &Scheduler{
		cron:         cron.New(),
		reminderSrv:  reminderSrv,
		attendanceUc: attendanceUc,
		userUc:       userUc,
	}
}

func (s *Scheduler) Start() {
	// Schedule the job to send clock-in reminders every day at a specific time (e.g., 9 AM).
	s.cron.AddFunc("0 9 * * *", s.sendClockInReminders)

	// Schedule the job to send clock-out reminders every day at a specific time (e.g., 6 PM).
	s.cron.AddFunc("0 18 * * *", s.sendClockOutReminders)

	// Start the cron scheduler
	s.cron.Start()
}

func (s *Scheduler) sendClockInReminders() {
	attendanceList, _ := s.attendanceUc.GetAttendances(time.Now().Truncate(24 * time.Hour))

	var userIds []int
	for _, v := range attendanceList {
		if v.ClockInTime == nil {
			userIds = append(userIds, v.ID)
		}
	}

	listRecipient, _ := s.userUc.GetUsersEmailUsernameByIds(userIds)

	// Use a channel to coordinate Goroutines and collect errors
	emailErrors := make(chan error, len(listRecipient))

	for _, v := range listRecipient {
		go func(user user.GetUserEmailUsernameModel) {
			if err := s.reminderSrv.SendClockInReminder(user.Email, user.Name); err != nil {
				log.Printf("Error sending clock-in reminder to %s: %v\n", user.Email, err)
				emailErrors <- err
			} else {
				log.Printf("Clock-in reminder sent to %s\n", user.Email)
			}
		}(v)
	}

	// Collect errors from Goroutines
	for i := 0; i < len(listRecipient); i++ {
		if err := <-emailErrors; err != nil {
			log.Printf("Clock-out error:\n%s", err.Error())
		}
	}
}

func (s *Scheduler) sendClockOutReminders() {
	attendanceList, _ := s.attendanceUc.GetAttendances(time.Now().Truncate(24 * time.Hour))

	var userIds []int
	for _, v := range attendanceList {
		if v.ClockOutTime == nil {
			userIds = append(userIds, v.ID)
		}
	}

	listRecipient, _ := s.userUc.GetUsersEmailUsernameByIds(userIds)

	// Use a channel to coordinate Goroutines and collect errors
	emailErrors := make(chan error, len(listRecipient))

	for _, v := range listRecipient {
		go func(user user.GetUserEmailUsernameModel) {
			if err := s.reminderSrv.SendClockOutReminder(user.Email, user.Name); err != nil {
				log.Printf("Error sending clock-in reminder to %s: %v\n", user.Email, err)
				emailErrors <- err
			} else {
				log.Printf("Clock-in reminder sent to %s\n", user.Email)
			}
		}(v)
	}

	// Collect errors from Goroutines
	for i := 0; i < len(listRecipient); i++ {
		if err := <-emailErrors; err != nil {
			log.Printf("Clock-out error:\n%s", err.Error())
		}
	}
}

func (s *Scheduler) Stop() {
	s.cron.Stop()
}
