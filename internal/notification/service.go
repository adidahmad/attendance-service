// internal/app/notification/service.go

package notification

import (
	"gitlab.com/adidahmad/attendance-service/config"

	"gopkg.in/gomail.v2"
)

type ReminderService struct {
	smtpConfig config.SmtpConfig
}

func NewReminderService(smtpConf config.SmtpConfig) *ReminderService {
	return &ReminderService{
		smtpConfig: smtpConf,
	}
}

func (s *ReminderService) SendClockInReminder(email, name string) error {
	// Compose the email message
	m := gomail.NewMessage()
	m.SetHeader("From", "your-email@example.com")                                                                     // Sender's email address
	m.SetHeader("To", email)                                                                                          // Recipient's email address
	m.SetHeader("Subject", "Clock-in Reminder")                                                                       // Email subject
	m.SetBody("text/plain", "Hello "+name+",\n\nIt's time to clock in for your work.\n\nBest regards,\nYour Company") // Email body

	// Create a new message with SMTP server details
	d := gomail.NewDialer(s.smtpConfig.SmtpServer, s.smtpConfig.SmtpPort, s.smtpConfig.SmtpUsername, s.smtpConfig.SmtpPassword)

	// Send the email
	if err := d.DialAndSend(m); err != nil {
		return err
	}

	return nil
}

func (s *ReminderService) SendClockOutReminder(email, name string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", "your-email@example.com")                                                                      // Sender's email address
	m.SetHeader("To", email)                                                                                           // Recipient's email address
	m.SetHeader("Subject", "Clock-out Reminder")                                                                       // Email subject
	m.SetBody("text/plain", "Hello "+name+",\n\nIt's time to clock out for your work.\n\nBest regards,\nYour Company") // Email body

	// Create a new message with SMTP server details
	d := gomail.NewDialer(s.smtpConfig.SmtpServer, s.smtpConfig.SmtpPort, s.smtpConfig.SmtpUsername, s.smtpConfig.SmtpPassword)

	// Send the email
	if err := d.DialAndSend(m); err != nil {
		return err
	}

	return nil
}
