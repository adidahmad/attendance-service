# Attendee Application

The Attendee Application is a Golang-based application for managing employee attendance with clock-in and clock-out features. It follows the principles of Clean Architecture, using the Echo framework for web handling and Entgo for ORM.

## Project Structure

The project follows a modular structure based on Clean Architecture principles:

```plaintext
attendee-application/
  │-── app/
  │   ├── handlers/
  │   │   ├── auth_handler.go (Authentication handler)
  │   │   ├── attendance_handler.go (Attendance handler)
  │   │   ├── user_handler.go (User handler)
  │   ├── middleware/
  │   │   ├── moddleware.go (Access Control handler)
  │   ├── util/
  │   │   ├── response.go (Response Formatter)
  │
  ├── cmd/
  │   ├── main.go (Application entry point)
  │
  ├── config/
  │   ├── config.go (Application configuration)
  │
  ├── ent/ (Entgo generated code)
  │
  ├── infrastructure/
  │   ├── database/
  │   │   ├── database.go (Database Connection)
  │
  ├── internal/
  │   ├── attendance/
  │   │   ├── usecase.go (Attendance use cases)
  │   │   ├── repository.go (Attendance repository interface)
  │   │   ├── model.go (Attendance data model)
  │   ├── auth/
  │   │   ├── usecase.go (Authentication use cases)
  │   │   ├── repository.go (Authentication repository interface)
  │   │   ├── model.go (Authentication data model)
  │   ├── notification/
  │   │   ├── service.go (Notification service)
  │   ├── scheduler/
  │   │   ├── service.go (Scheduler service)
  │   ├── user/
  │   │   ├── usecase.go (User use cases)
  │   │   ├── repository.go (User repository interface)
  │   │   ├── model.go (User data model)
  | 
  ├── pkg/
  │   ├── server/
  │   │   ├── server.go (Echo server setup)
  │   ├── jwt/
  │   │   ├── jwt.go (JWT utility functions)
  │
  ├── config.yaml (Config Env)
```

## Getting Started
#### Install Dependencies:

1. Adjust `config.yaml` based on your preferred config. (existing config already)

2. Set Up go mod:
```
go mod tidy
```

3. Run the Application:
```
go run cmd/main.go
```
Visit http://localhost:1125/api to access the application.

#### Postman Collection
kindly import this file `Attendance Service.postman_collection.json` to your postman desktop app
